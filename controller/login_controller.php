<?php

if(isset($_POST['submit_login'])){

	$user_email = trim($_POST['email']);
	$user_password = trim($_POST['password']);

	try{

		$db_con = $blog_bd->prepare('SELECT * FROM users WHERE email=:email');
		$db_con->execute(array(':email'=>$user_email));

		$row = $db_con->fetch(PDO::FETCH_ASSOC);
		$count = $db_con->rowCount();

		if( $row['password'] == $user_password ){

			if( isset($_POST['remember_me']) ){

				$logged_token = md5(mt_rand() . $row['user_id'] . $row['name'] . $row['rank'] . $row['email'] . $row['password']);

				setcookie( 'loged', $logged_token, time()+2592000);

				$db_con = $blog_bd->prepare( 'UPDATE users SET loged_token="' . $logged_token . '" WHERE user_id =' . $row['user_id'] );
				$db_con->execute();

			}

			$_SESSION['user_session'] = $row['user_id'];
			header('location:/view/site/home.php');

		} else{
			echo "Incorrect input data, email or password!";
		}

	}catch(PDOException $e){
		echo $e->getMessage();
	}
}
