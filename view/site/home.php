<?php
require_once('header.php');
?>
<?php 


$user_id = intval($_SESSION['user_session']);
$admin_rank_check = 'admin';

try {
	$db_con = $blog_bd->prepare('SELECT * FROM users WHERE user_id ='.$user_id);
	$db_con->execute();

	while ($row = $db_con->fetch()) {
		$user_name = $row['name'];
		$user_rank = $row['rank'];
	}

} catch (PDOException $e) {
	echo $e->getMessage;
}

?>
<header>

	<nav>
		<div class="col-12">
			<h1>  Helloo <?php echo $user_name . '<br> <small>Your rank is - </small>' . $user_rank;?></h1>
			<form id="logout_form" action="" method="post" >
				<button type="submit" name="submit_log_out" id="" class="btn btn-danger">Log Out</button>
				<?php if( $user_rank == $admin_rank_check){ ?>
					<button type="submit" name="to_admin_panel" class="btn btn-primary"> Admin Panel </button>
					
					<?php } ?>
				</form>
			</div>
		</nav>
	</header>
	<div class="clearfix"></div>
	<?php

	if( isset($_POST['to_admin_panel']) ){
		header('location:/view/admin/dashboard.php');
	}

	if(isset($_POST['submit_log_out'])){
		session_destroy();
		session_unset();
		setcookie('loged', null, -1, '/');
		$db_con = $blog_bd->prepare( 'UPDATE users SET loged_token="" WHERE user_id = ' . $user_id );
		$db_con->execute();
		header('location:/');
	}



	require_once('post_row.php');
	require_once('footer.php');
	?>