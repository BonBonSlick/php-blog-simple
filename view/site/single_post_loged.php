<?php
require_once('header.php');
?>
<?php 


$user_id = intval($_SESSION['user_session']);
$admin_rank_check = 'admin';

try {
	$db_con = $blog_bd->prepare('SELECT * FROM users WHERE user_id ='.$user_id);
	$db_con->execute();

	while ($row = $db_con->fetch()) {
		$user_name = $row['name'];
		$user_rank = $row['rank'];
	}

} catch (PDOException $e) {
	echo $e->getMessage;
}

?>
<header>

	<nav>
		<div class="col-12">
			<h1>  Helloo <?php echo $user_name . '<br> <small>Your rank is - </small>' . $user_rank;?></h1>
			<form id="logout_form" action="" method="post" >
				<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>/view/site/home.php"><div  class="btn btn-success"><b><-Back</b></div></a>
				<button type="submit" name="submit_log_out" id="" class="btn btn-danger">Log Out</button>
				<?php if( $user_rank == $admin_rank_check){ ?>
				<button type="submit" name="to_admin_panel" class="btn btn-primary"> Admin Panel </button>

				<?php } ?>
			</form>
		</div>
	</nav>
</header>

<?php 
$post_slug = $_GET['post_slug'];

try {
	$db_con = $blog_bd->prepare('SELECT * FROM posts WHERE post_slug = :post_slug');
	$db_con->execute(array(
		':post_slug' => $post_slug
		));

	while($row = $db_con->fetch()){
			echo '<h1>'. $row['post_title'] . '</h1>';
			if ( !empty($row['post_pic_url'] ) ) {
				echo "<img src='" . $row['post_pic_url'] . "' alt='' width='500px'  height='500px'>". '<br>';
			}
	
			echo $row['post_cont'] . '<br>';
			echo $row['post_date'] . '<br>';
			$db_con_2 = $blog_bd->prepare('SELECT * FROM post_cats WHERE post_id = ' . $row['post_id']);
			$db_con_2->execute();
			while ( $row = $db_con_2->fetch() ) {
				echo $post_cat = $row['cat_name'];
			}
		}

} catch (PDOException $e) {
	echo $e->getMessage();
}



?>

<div class="clearfix"></div>
<?php

if( isset($_POST['to_admin_panel']) ){
	header('location:/view/admin/dashboard.php');
}

if(isset($_POST['submit_log_out'])){
	session_destroy();
	session_unset();
	setcookie('loged', null, -1, '/');
	$db_con = $blog_bd->prepare( 'UPDATE users SET loged_token="" WHERE user_id = ' . $user_id );
	$db_con->execute();
	header('location:/');
}

require_once('footer.php');
?>