<?php 
if( isset($_COOKIE['loged']) && !empty($_COOKIE['loged'])  ){
	header('location:/view/site/home.php');
	exit;
} 

require_once($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/controller/login_controller.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/controller/register_controller.php');

if ( isset ( $_SESSION['user_session'] )  || !empty( $_SESSION['user_session'] ) ){
	header('location:/view/site/home.php');
	exit;
}
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head -->
	<meta name="keywords" content="keywords" />
	<meta name="description" content="description" />
	<meta name="author" content="">
	<title><?php echo substr(basename($_SERVER['PHP_SELF']), 0, -4);?></title>
	<!-- Свой JS -->
	<script type="text/javascript" src="../../assets/js/jquery.min.js" ></script>
	<!-- <script type="text/javascript" src="../../assets/js/login_ajax.js" ></script> -->
	<!-- Свой CSS -->
	<link rel="stylesheet" type="text/css" href="../../assets/css/site.css">
</head>
<body role="document">
	<header>
		<nav>
			<div class="col-12"> 
				<?php 
// var_dump($_SERVER); 
				?>
				<a href="http://<?php echo $_SERVER['HTTP_HOST'];?>"><div  class="btn btn-success"><b><-Back</b></div></a>
				<div id="login_btn" class="btn btn-primary">Log In</div>

				<form id="login_form" action="" method="post" >
					<div id="btn_back_login" class="btn btn-sm btn-danger"> Back </div>
					<input type="email" name="email" placeholder="Email" required>
					<input type="password" name="password" placeholder="Password" required>
					<input type="checkbox"  name="remember_me" id="" value="0">
					Remmeber Me
					<button type="submit" name="submit_login" id="submit_login" class="btn btn-success">Log In</button>
				</form>

				<div id="register_btn" class="btn btn-danger">Register Now!</div>

				<form id="register_form" action="" method="post" >
					<div id="btn_back_login" class="btn btn-sm btn-danger"> Back </div>
					<input type="text" name="name" placeholder="Your Name" required>
					<input type="email" name="email" placeholder="Email" required>
					<input type="password" name="password" placeholder="Password" required>
					<button type="submit" name="submit_register" id="submit_register" class="btn btn-success">Register!</button>
				</form>

			</div>


		</nav>
	</header>

	<?php 
	$post_slug = $_GET['post_slug'];

	try {
		$db_con = $blog_bd->prepare('SELECT * FROM posts WHERE post_slug = :post_slug');
		$db_con->execute(array(
			':post_slug' => $post_slug
			));

		while($row = $db_con->fetch()){
			echo '<h1>'. $row['post_title'] . '</h1>';
			echo $row['post_cont'] . '<br>';
			echo $row['post_date'] . '<br>';
			$db_con_2 = $blog_bd->prepare('SELECT * FROM post_cats WHERE post_id = ' . $row['post_id']);
			$db_con_2->execute();
			while ( $row = $db_con_2->fetch() ) {
				echo $post_cat = $row['cat_name'];
			}
		}

	} catch (PDOException $e) {
		echo $e->getMessage();
	}



	?>

	<script type="text/javascript">

		$(document).on('click', '#register_btn', function(){
			$('#register_form').show();
			$('#login_btn').hide();
			$('#register_btn').hide();
		});

		$(document).on('click', '#login_btn', function(){
			$('#login_form').show();
			$('#login_btn').hide();
			$('#register_btn').hide();
		});

		$(document).on('click', '#btn_back_login', function(){
			$('#register_form').hide();
			$('#login_form').hide();
			$('#login_btn').show();
			$('#register_btn').show();
		});

	</script>
	
	<?php
	require_once('../site/sidebar.php');
	require_once('../site/footer.php');
