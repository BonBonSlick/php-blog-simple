<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');

$user_id = intval($_SESSION['user_session']);
$admin_rank_check = 'admin';

try {
	$db_con = $blog_bd->prepare('SELECT * FROM users WHERE user_id ='.$user_id);
	$db_con->execute();

	while ($row = $db_con->fetch()) {
		$user_name = $row['name'];
		$user_rank = $row['rank'];
		if( $user_rank !== $admin_rank_check){  
			header('location:/');
		}  
	}

} catch (PDOException $e) {
	echo $e->getMessage;
}

?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head -->
	<meta name="keywords" content="keywords" />
	<meta name="description" content="description" />
	<meta name="author" content="">
	<title><?php echo substr(basename($_SERVER['PHP_SELF']), 0, -4);?></title>
	<!--  CSS -->
	<link rel="stylesheet" type="text/css" href="../../assets/css/site.css">
	<link rel="stylesheet" type="text/css" href="../../assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/site.css">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/font-awesome.min.css">
	<!--  JS -->
	<script type="text/javascript" src="/assets/js/jquery.min.js" ></script>
 	<!-- <script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script> -->
</head>
<body role="document">



	<div class="container-fluid">
		<div class="row">
			<div class="col-2 ">
				<?php require_once($_SERVER['DOCUMENT_ROOT'] . "/view/admin/nav.php" ); ?>
			</div>
			<div class="col-10" id="admin-navigator">
				<div class="col-4"  >
					Welcome: <b><?php echo $user_name;?></b>
				</div>
				<div class="col-8">
					<form id="logout_form" class="pull-right" action="" method="post" >
						<button type="submit" name="submit_log_out" id="" class="btn btn-danger">Log Out</button>
					</form>
				</div>
			</div>

		</div>
	</div>
	<?php 

	if(isset($_POST['submit_log_out'])){
		session_destroy();
		session_unset();
		setcookie('loged', null, -1, '/');
		$db_con = $blog_bd->prepare( 'UPDATE users SET loged_token="" WHERE user_id = ' . $user_id );
		$db_con->execute();
		header('location:/');
	}


	?>

