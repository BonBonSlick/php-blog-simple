<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/admin/model_edit_cat.php');
?>
<script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script>
<div class="row">
	<div class="col-10">
		<h1>Cat ID: <?php echo $_GET['cat_id_to_edit']; ?></h1>

	</div>
</div>
<hr>
<?php 
require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');
try {
	// PCategory information
	$cat_id = $_GET['cat_id_to_edit'];

		// Post Category
	$db_con = $blog_bd->prepare('SELECT * FROM categories WHERE cat_id = ' . $cat_id);
	$db_con->execute();


	while ($row = $db_con->fetch()  ) {
		$cat_id = $row['cat_id'];
		$cat_name = $row['cat'];
	}

} catch (PROException $e) {
	$e->getMessage();
}
?>
<div class="col-10">

	<form action="" method="post">

		Title
		<input type="text" name="new_cat_title" value="<?php echo $cat_name; ?>" placeholder="Type here category title"  size="" autocomplete="on"> <br>

		<button type="submit" name="submit_edit_cat" class="btn btn-success">Save</button>
	</form>
</div>
<?php 
submit_edit();
?>
