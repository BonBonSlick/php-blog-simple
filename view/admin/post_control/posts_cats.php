<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/admin/model_all_cats.php');
?>
<script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script>
<div class="row">
	<div class="col-10">
		<div class="col-3">
			<form method="post" action="add_cat.php">
				<b>Post Categories</b> 
				<button  type="submit" name="add_new_post" class="btn btn-primary"> Add New Category</button>
			</form>
		</div>
		<div class="col-6">
			<form method="get" action="search_results.php?<?php echo $_GET['search_data_field'] . $_GET['search_data_type'];?>">Search By Category:
				<select name="search_data_type" id="">
					<option value="post_id">ID</option>
					<option value="post_title">Title</option>
				</select>
				<input type="search" name="search_data_field" placeholder="Category: ID / Title " size="30">
				
				<button type="submit"  class="btn btn-primary">
					Search
				</button>    
			</form>
		</div>
		<div class="col-3">
			<form method="post">
				<button type="button" class="btn btn-success show-filter">Show Filter</button> 
				<button type="button" class="btn btn-success hide-filter">Hide Filter</button> 
				<button type="button" class="btn btn-primary clear-filter">Clear Filter</button> 
			</form>
		</div> 
	</div>
</div>
<hr>
<div class="table-responsive">
	<form method='post' action=''> 
		<table  >
			<thead>
				<tr>
					<th >
						<input type='checkbox' class='check_all' />
						<button type='submit' name='btn-delete-all' value="AAAAA" class='btn btn-danger'>DELETE SELECTED </button>
					</th>
					<th class="sort-by" >
						#ID <i class="fa fa-long-arrow-down"></i>
					</th>
					<th class="sort-by">
						Title <i class="fa fa-long-arrow-down"></i>
					</th>
					<th >
						Action 
					</th>
				</tr>
			</thead>

			<tbody>
				<?php show_table();?>
			</tbody>
		</table>
		<div class="col-12"> 
			<div class="row">
				<div class="col-5">
					<form action="" method="post">
						<button type="submit" name="submit_post_per_page" class="btn btn-primary" > Show </button>
						<select name="post_per_page" id="">
							<option><?php
								echo $_SESSION['post_per_page'];
								?></option>
								<option value="1">1</option>
								<option value="5">5</option>
								<option value="25">25</option>
								<option value="50">50</option>
								<option value="100">100</option>
								<option value="250">250</option>
							</select>
						</form>
					</div>
					<div class="col-7">
						<?php pagination();  ?>
					</div>
				</div>
			</div>
		</form>
	</div>
	<?php buttons_controls(); ?>
	<div class="clearfix"></div>
	<hr>
	<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/footer.php');
	?>
