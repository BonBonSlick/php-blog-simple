<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/admin/model_edit_post.php');
?>
<script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script>
<div class="row">
	<div class="col-10">  
		<h1>Post ID: <?php echo $_GET['post_id_to_edit']; ?></h1> 
	</div>
</div>
<hr>
<?php 
require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');
try {
	// Post information
	$post_id = $_GET['post_id_to_edit'];
	$db_con = $blog_bd->prepare('SELECT * FROM posts WHERE post_id = ' . $post_id);
	$db_con->execute();
	while ( $row = $db_con->fetch()) {
		$post_title = $row['post_title'];
		$post_slug = $row['post_slug'];
		$post_cont = $row['post_cont'];
		$post_pic_url = $row['post_pic_url'];
	}
		// Post Category
	$db_con_2 = $blog_bd->prepare('SELECT * FROM post_cats WHERE post_id = ' . $post_id);
	$db_con_2->execute();
	while ( $row = $db_con_2->fetch() ) {
		$post_cat = $row['cat_name'];
	}
		// All Categories
	$db_con_3 = $blog_bd->prepare('SELECT * FROM categories');
	$db_con_3->execute();
} catch (PROException $e) {
	$e->getMessage();
}
?>
<div class="col-10">

	<script type="text/javascript">

		jQuery(document).ready(function($) {



			$(document).on('click', '#delete_img', function(){

				$('#old_post_pic').attr('src', '/assets/img/no_img.png');
				$('#hidden_pic_field').val('');

			});

			function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#old_post_pic').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				}
			} 

			if ($('#old_post_pic').attr('src') == ''){
				$('#old_post_pic').attr('src', '/assets/img/no_img.png');
			}  

			$('body').on('change','#img_inp',function(){
				readURL(this); 
			});

		});

	</script>

	<form action="" method="post" enctype="multipart/form-data" >	

		Post Picture
		<input type="file" accept="image/*" name="img_inp"  id="img_inp" style="visibility:hidden ;" > <br>
		<input type="hidden" value="<?php echo $post_pic_url; ?>" name="old_post_pic">

		<img src="<?php echo $post_pic_url; ?>"  id="old_post_pic" alt="Post IMG" width="250px" height="250px">	<br>

		<div class="col-4">
			<div class="col-3 btn btn-danger" id="delete_img" >Delete  </div>
			<div class="col-3 btn btn-success" onclick="$('#img_inp').click()">Change</div>
		</div> <br>  <br>



		Title
		<input type="text" name="new_post_title" value="<?php echo $post_title; ?>" placeholder="Type here post title"  size="" autocomplete="on"> <br>
		Slug
		<input type="text" name="new_post_slug" value="<?php echo $post_slug; ?>" placeholder="Post slug for link"  size="" autocomplete="on"> <br>
		Content
		<textarea name="new_post_cont" id="" cols="25" rows="3" minlength="5" maxlength="9999" placeholder="Type here content of post"><?php echo $post_cont; ?></textarea> <br>
		Post Category
		<select name="new_post_cat" >
			<option selected value="<?php echo $post_cat; ?>"><?php echo $post_cat; ?></option>
			<option  value="">No Category</option>
			<?php 
			while ( $row = $db_con_3->fetch() ) {
				$post_cat = $row['cat'];
				echo '<option value="' . $post_cat . '">' . $post_cat . ' </option>';
			}
			?>
		</select>   
		<button type="submit" name="submit_edit_post" class="btn btn-success">Save</button>
	</form>
</div>
<?php 
submit_edit();
?>
