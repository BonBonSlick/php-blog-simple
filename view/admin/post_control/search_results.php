<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/admin/model_all_posts.php');
?>
<script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script>
<div class="row">
	<div class="col-10">
		<div class="col-3">
			<form method="post" action="add_post.php">
				<b>Search Result</b> 
				<button  type="submit" name="add_new_post" class="btn btn-primary"> Add New Post</button>
			</form>
		</div>
		<div class="col-6">
			<form method="get" action="search_results.php?<?php echo $_GET['search_data_field'] . $_GET['search_data_type'];?>">Search By Post:
				<select name="search_data_type" id="">
					<option value="post_id">ID</option>
					<option value="post_title">Title</option>
					<!-- <option value="">Category</option> -->
					<option value="post_date">Date</option>
				</select>
				<input type="search" name="search_data_field" placeholder="Post: ID / Title / Date" size="30">
				
				<button type="submit"  class="btn btn-primary">
					Search
				</button>    
			</form>
		</div>
		<div class="col-3">
			<form method="post">
				<button type="button" class="btn btn-success show-filter">Show Filter</button> 
				<button type="button" class="btn btn-success hide-filter">Hide Filter</button> 
				<button type="button" class="btn btn-primary clear-filter">Clear Filter</button> 
			</form>
		</div> 
	</div>
</div>
<hr>
<div class="table-responsive">
	<form method='post' action=''> 
		<table  >
			<thead>
				<tr>
					<th >
						<input type='checkbox' class='check_all' />
						<button type='submit' name='btn-delete-all' value="AAAAA" class='btn btn-danger'>DELETE SELECTED </button>
					</th>
					<th class="sort-by" >
						#ID <i class="fa fa-long-arrow-down"></i>
					</th>
					<th class="sort-by">
						Title <i class="fa fa-long-arrow-down"></i>
					</th>
					<th class="sort-by">
						Content <i class="fa fa-long-arrow-down"></i>
					</th>
					<th class="sort-by">
						Category <i class="fa fa-long-arrow-down"></i>
					</th>
					<th class="sort-by">
						Post Date <i class="fa fa-long-arrow-down"></i>
					</th>
					<th >
						Action 
					</th>
				</tr>
			</thead>

			<tbody>
				<?php

				$search_data =  $_GET['search_data_field'];
				$search_data_type =  $_GET['search_data_type'];

				try {
					$db_con = $blog_bd->prepare('SELECT * FROM posts WHERE ' . $search_data_type  . ' =:search_data');
					$db_con->execute(array(
						':search_data' =>  $search_data
						));



					while ( $row = $db_con->fetch()) {
						$post_id = $row['post_id'];
						$post_title = $row['post_title'];
						$post_slug = $row['post_slug'];
						$post_cont = $row['post_cont'];
						$post_date = $row['post_date'];


						$db_con_2 = $blog_bd->prepare('SELECT * FROM post_cats WHERE post_id = ' . $post_id);
						$db_con_2->execute();

						echo "
						<tr>
							<td><input type='checkbox'  name='checkme[]'   value='" .  $post_id . "' />
								Select to Delete
							</td>
							<td> "  . $post_id . " </td>
							<td> "  . $post_title . " </td>
							<td> "  . $post_cont . " </td><td> ";

							while ( $row = $db_con_2->fetch() ) {
								echo $post_cat = $row['cat_name'];
							}

							echo "</td><td> "  . $post_date . " </td>
							<td>

								<a href='#'><button type='button' name='btn-show' class='btn btn-primary btn-show'>Show</button></a>
								<a href='http://blog/view/admin/post_control/edit_post.php?post_id_to_edit=" .  $post_id . "'>
									<div class='btn btn-success btn-edit'>Edit</div>
								</a>
								<button type='submit' name='btn-remove' value='" .  $post_id . "'class='btn btn-danger btn-remove'>Remove</button>

							</td>
						</tr>
						";

					}



				} catch (PDOException $e) {
					$e->getMessage();
				}


				?>
			</tbody>
		</table>

	</form>
</div>
<?php buttons_controls(); ?>
<div class="clearfix"></div>
<hr>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/footer.php');
?>
