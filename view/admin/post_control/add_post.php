<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/admin/model_add_post.php');
?>
<script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script>
<div class="row">
	<div class="col-10">
		<h1>Adding Post</h1>

	</div>
</div>
<hr>

<div class="col-10">

	<script type="text/javascript">
		jQuery(document).ready(function($) {

			function readURL(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#img_preview').attr('src', e.target.result);
					}
					reader.readAsDataURL(input.files[0]);
				}
			} 

			$('body').on('change','#img_inp',function(){
				readURL(this); 
			});

			if ($('#img_preview').attr('src') == ''){
				$('#img_preview').attr('src', '/assets/img/no_img.png');
			}  
		});
	</script> 


	<form   method="post" enctype="multipart/form-data" accept="img/*" >
		<img id="img_preview" src="" alt="Post Image Prewie" height="200px"  width="200px" /> <br>
		<input type='file'  name="image"  id="img_inp" /><br>
		
		Title
		<input type="text" name="new_post_title" value="<?php echo $post_title; ?>" placeholder="Type here post title"  size="" autocomplete="on"> <br>
		Slug
		<input type="text" name="new_post_slug" value="<?php echo $post_slug; ?>" placeholder="Post slug for link"  size="" autocomplete="on"> <br>
		Content
		<textarea name="new_post_cont" id="" cols="25" rows="3" minlength="5" maxlength="9999" placeholder="Type here content of post"><?php echo $post_cont; ?></textarea> <br>
		Post Category
		<select name="new_post_cat" >
			<option selected value="<?php echo $post_cat; ?>"><?php echo $post_cat; ?></option>
			<option  value="">No Category</option> -->
			<?php 
			try {
		// All Categories
				$db_con_3 = $blog_bd->prepare('SELECT * FROM categories');
				$db_con_3->execute();
			} catch (PROException $e) {
				$e->getMessage();
			}
			while ( $row = $db_con_3->fetch() ) {
				$post_cat = $row['cat'];
				echo '<option value="' . $post_cat . '">' . $post_cat . ' </option>';
			}
			
			?>
		</select> 
		<button type="submit" name="submit_add_post" class="btn btn-success">Add This Post</button>
	</form>
</div>
<?php
submit_post();
?>
