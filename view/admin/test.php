<?php
if(isset($_FILES['image'])){
	$errors= array();
	$file_name = $_FILES['image']['name'];
	$file_size = $_FILES['image']['size'];
	$file_tmp = $_FILES['image']['tmp_name'];
	$file_type = $_FILES['image']['type'];
	$file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

	$expensions= array("jpeg","jpg","png");

      // if(in_array($file_ext,$expensions)=== false){
      //    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
      // }

      // if($file_size > 2097152) {
      //    $errors[]='File size must be excately 2 MB';
      // }

	if(empty($errors)==true) {
		move_uploaded_file($file_tmp, $_SERVER['DOCUMENT_ROOT'] . "/assets/img/" . $file_name);
		echo "Success";
	}else{
		print_r($errors);
	}
}
?>

<!DOCTYPE html>
<head>

	<!-- Свой JS -->
	<script type="text/javascript" src="../../assets/js/jquery.min.js" ></script>

</head>
<body role="document">
	<body>

<!-- 	<form action = "" method = "POST" enctype = "multipart/form-data">
		<input type = "file" name = "image" accept="image/*" />
		<input type = "submit"/>

		<ul>
			<li>Sent file: <?php echo $_FILES['image']['name'];  ?></li>
			<li>File size: <?php echo $_FILES['image']['size'];  ?></li>
			<li>File type: <?php echo $_FILES['image']['type'] ?></li>
		</ul>

	</form>
-->   


<script type="text/javascript">
	jQuery(document).ready(function($) {

		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#img_preview').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		} 

		$(document).ready(function(){
			$('body').on('change','#imgInp',function(){
				readURL(this); 
			});
			
		});
	});
</script> 


<form id="form1" runat="server">
	<input type='file' id="imgInp" />
	<img id="img_preview" src="#" alt="your image" height="100px"  width="100px" />
</form>

</body>
</html>