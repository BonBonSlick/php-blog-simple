<nav>
	
	<ul class="navbar">
		<a href="/">
			<li> SITE LINK</li>
		</a>

		<a href="/view/admin/dashboard.php">
			<li id="show_dash">
				Dashboard
				<span class="fa fa-area-chart pull-right"></span>
			</li>
		</a> 

		<li class="multi-nav">
			Posts
			<span class="fa fa-th pull-right" aria-hidden="true"></span>
			<ul class="multi-nav-hide">
				<a href="/view/admin/post_control/all_posts.php">
					<li id="show_all_posts">
						  Posts 
					</li>
				</a> 
				<a href="/view/admin/post_control/posts_cats.php">
					<li id="show_all_cats">
						  Categories
					</li>
				</a>
				
			</ul>
		</li>

		<li class="multi-nav">
			Users
			<span class="fa fa-users pull-right" aria-hidden="true"></span>
			<ul class="multi-nav-hide">
				<a href="/view/admin/user_control/all_users.php">
					<li id="show_all_users">  Users</li>
				</a>
				<a href="/view/admin/user_control/user_ranks.php">
					<li id="show_all_ranks">  Ranks</li>
				</a>	
			</ul>
		</li>
	</ul>
</nav>

