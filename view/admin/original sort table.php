<table>
  <thead>
    <tr>
      <th class="sort" >#ID <i class="fa fa-long-arrow-down"></i></th>
      <th class="sort">Title <i class="fa fa-long-arrow-down"></i></th>
      <th class="sort">Content <i class="fa fa-long-arrow-down"></i></th>
    </tr>
  </thead>
  <tbody>
    <tr><td>111</td><td>1.1</td><td>false</td></tr>
    <tr><td>111</td><td>1.2</td><td>false</td></tr>
    <tr><td>111</td><td>1.2</td><td>tes</td></tr>
    <tr><td>1</td><td>1.3</td><td>text</td></tr>
    <tr><td>test1</td><td>tesst1</td><td>tess</td></tr>
    <tr><td>test2</td><td>tesst2</td><td>test</td></tr>
    <tr><td>test3</td><td>test3</td><td>text</td></tr>
    <tr><td>test3</td><td>-200</td><td>text</td></tr>
    <tr><td>test3</td><td>-2</td><td>text</td></tr>
    <tr><td>333</td><td>-2</td><td>text</td></tr>
    <tr><td>222</td><td>-2</td><td>22</td></tr>

  </tbody>
</table>



<script type="text/javascript">
jQuery(document).ready(function($) {
  
  $('th').click(function(){
    var table = $(this).parents('table').eq(0)
    var rows = table.find("tr:not(:has('th'))").toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc){
      rows = rows.reverse()
    }
        for (var i = 0; i < rows.length; i++){
      table.append(rows[i])
    }
  });

  function comparer(index) {
        return function(a, b) {
              var valA = getCellValue(a, index), valB = getCellValue(b, index)
              return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
        }
  }

  function getCellValue(row, index){ return $(row).children('td').eq(index).html() }

// additional code to apply a filter
$('table').each(function(){
      var table = $(this)
      var headers = table.find('th').length
      var filterrow = $('<tr>').insertAfter($(this).find('th:last()').parent())
      for (var i = 0; i < headers; i++){
            filterrow.append($('<th>').append($('<input>').attr('type','text').keyup(function(){
            table.find('tr').show()
                 filterrow.find('input[type=text]').each(function(){
                  var index = $(this).parent().index() + 1
                   var filter = $(this).val() != ''
                   $(this).toggleClass('filtered', filter)
                  if (filter){
                        var el = 'td:nth-child('+index+')'
                        var criteria = ":contains('"+$(this).val()+"')"
                        table.find(el+':not('+criteria+')').parent().hide()
                  }
                 })
           })))
      }
  
      filterrow.append($('<th>').append($('<input>').attr('type','button').val('Clear Filter').click(function(){
            $(this).parent().parent().find('input[type=text]').val('').toggleClass('filtered', false)
            table.find('tr').show()
      })))
})


});
</script>