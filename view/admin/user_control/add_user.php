<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/view/admin/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/model/admin/model_edit_post.php');
?>
<script type="text/javascript" src="/assets/js/table_sort_filter.js" ></script>
<div class="row">
	<div class="col-10">
		<div class="col-4">
			<form method="post" action="">
				<b>Adding Post</b> 
				<button  type="submit" name="add_new_post" class="btn btn-primary"> Add New Post</button>
			</form>
		</div>
		<div class="col-4">
			<form method="post" action="">
				<input type="text" name="search_data_field">
				<button type="submit" name="submit_search_data" class="btn btn-primary">
					Search
				</button>    
			</form>
		</div>
		<div class="col-4">
			<form method="post">
				<button type="button" class="btn btn-success show-filter">Show Filter</button> 
				<button type="button" class="btn btn-success hide-filter">Hide Filter</button> 
				<button type="button" class="btn btn-primary clear-filter">Clear Filter</button> 
			</form>
		</div> 
	</div>
</div>
<hr>
<?php 
try {

		// All Categories
	$db_con_3 = $blog_bd->prepare('SELECT * FROM categories');
	$db_con_3->execute();
} catch (PROException $e) {
	$e->getMessage();
}
?>
<div class="col-10">

	<form action="" method="post">
		<!-- Picture
		<input type="file" accept="image/*" name="" value="" placeholder=""> <br> -->
		<!-- <input type="image" src=""   alt="image"> <br> -->
		Title
		<input type="text" name="new_post_title" value="<?php echo $post_title; ?>" placeholder="Type here post title"  size="" autocomplete="on"> <br>
		Slug
		<input type="text" name="new_post_slug" value="<?php echo $post_slug; ?>" placeholder="Post slug for link"  size="" autocomplete="on"> <br>
		Content
		<textarea name="new_post_cont" id="" cols="25" rows="3" minlength="5" maxlength="9999" placeholder="Type here content of post"><?php echo $post_cont; ?></textarea> <br>
		Post Category
		<select name="new_post_cat" >
			<option selected value="<?php echo $post_cat; ?>"><?php echo $post_cat; ?></option>
			<option  value="">No Category</option>
			<?php 
			while ( $row = $db_con_3->fetch() ) {
				$post_cat = $row['cat'];
				echo '<option value="' . $post_cat . '">' . $post_cat . ' </option>';
			}
			?>
		</select> 
		<button type="submit" name="submit_edit_post" class="btn btn-success">Add This Post</button>
	</form>
</div>
<?php 
if ( isset($_POST['submit_edit_post'])){
	try {
		// Post information
		$db_con = $blog_bd->prepare('INSERT INTO posts ( post_id, post_title, post_slug, post_cont, post_date ) VALUES ( post_id, :post_title,  :post_slug,  :post_cont, :post_date ) ');
		$db_con->execute(array(
			':post_title' => $_POST['new_post_title'],
			':post_slug' => $_POST['new_post_slug'],
			':post_cont' => $_POST['new_post_cont'],
			':post_date' => date('Y-m-d H:i:s')
			));

		$new_post_id =  $blog_bd->lastInsertId();

		// Post Category
		$db_con_2 = $blog_bd->prepare('INSERT INTO post_cats ( id, cat_name, post_id ) VALUES ( id, :cat_name,  :post_id ) ');
		$db_con_2->execute(array(
			':cat_name' => $_POST['new_post_cat'],
			':post_id' =>  $new_post_id 
			));
	} catch (PDOException $e) {
		$e->getMessage();
	}
	header('location:all_posts.php');
}
?>
