/* Commands 
SELECT `cat_id` FROM `post_cats` WHERE cat_id = 1
INSERT INTO `post_cats`(`cat_id`, `cat`, `cat_posts`) VALUES ('222','[value-2]','123')
UPDATE `post_cats` SET`cat_posts`=('222') WHERE cat_id = 1
DELETE FROM `post_cats` WHERE cat_id = 1
*/
DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `rank` varchar(255) DEFAULT NULL,
  `avatar_url` text DEFAULT NULL,
  `loged_token` text DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

LOCK TABLES `users` WRITE;

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `rank`)
VALUES 
(1,'Admin_Name','admin@admin.com', 'admin','admin'),
(2,'User_Name','user@user.com','user','user');

UNLOCK TABLES;


DROP TABLE IF EXISTS `user_ranks`;

CREATE TABLE `user_ranks` (
  `rank_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

LOCK TABLES `user_ranks` WRITE;

INSERT INTO `user_ranks` (`rank_id`, `rank` )
VALUES 
(1,'admin' ),
(2,'user' );

UNLOCK TABLES;


DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `post_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_title` varchar(255) DEFAULT NULL,
  `post_slug` varchar(255) DEFAULT NULL,
  `post_cont` text,
  `post_pic_url` text,
  `post_date` datetime DEFAULT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `posts` WRITE;

INSERT INTO `posts` (`post_id`, `post_title`, `post_slug`, `post_cont`, `post_date`)
VALUES
(1 , 'Test Title 1', 'testtitle123rfsda3rfas', 'Here goes some text, the content of this 111111  post!', '2011-09-29 06:12:44'),
(2 , 'Test Title 2', 'testtitle2f3qwafsv3a4wr', 'Here goes some text, the content of this 222222 post!', '2012-01-15 12:53:25'),
(3 , 'Test Title 3', 'testtitle234fva4w5t24g', 'Here goes some text, the content of this 333333 post!', '2013-06-22 6:23:29'),
(4 , 'Test Title 4', 'testtitle4aggfv3vsfx', 'Here goes some text, the content of this 444444 post!', '2013-06-22 6:23:29'),
(5 , 'Test Title 5', 'testtitle534tgfzvsg', 'Here goes some text, the content of this 555555 post!', '2013-06-22 6:23:29'),
(6 , 'Test Title 6', 'testtitle6qegaffdjah', 'Here goes some text, the content of this 666666 post!', '2013-06-22 6:23:29'),
(7 , 'Test Title 7', 'testtitle7bstrjshgvser', 'Here goes some text, the content of this 777777 post!', '2013-06-22 6:23:29'),
(8 , 'Test Title 8', 'testtitle8nwrtshrytq3fafs', 'Here goes some text, the content of this 888888 post!', '2013-06-22 6:23:29');
 
UNLOCK TABLES;

DROP TABLE IF EXISTS `post_cats`;

CREATE TABLE `post_cats` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(55) DEFAULT NULL,
  `cat_name` varchar(255) DEFAULT NULL,
  `post_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

LOCK TABLES `post_cats` WRITE;

INSERT INTO `post_cats` (`id`, `cat_id`, `cat_name`, `post_id` )
VALUES 
( 1, 2, 'test cat 2', 2),
( 2, 1, 'test cat 1', 3),
( 3, 3, 'test cat 3', 1),
( 4, 1, 'test cat 1', 4),
( 5, 1, 'test cat 1', 5),
( 6, 3, 'test cat 3', 6),
( 7, 2, 'test cat 2', 7),
( 8, 2, 'test cat 2', 8);



UNLOCK TABLES;


DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `cat_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ;

LOCK TABLES `categories` WRITE;

INSERT INTO `categories` (`cat_id`, `cat`  )
VALUES 
(1,'test cat 1' ),
(2,'test cat 2' ),
(3,'test cat 3' );

UNLOCK TABLES;

