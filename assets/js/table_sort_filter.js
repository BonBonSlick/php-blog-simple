jQuery(document).ready(function($) {


    // Sort rows by ASC od DESC by clicking 
    $(document).on('click', '.sort-by', function() {
      var table = $(this).parents('table').eq(0);
      var rows = table.find("tr:not(:has('th'))").toArray().sort(comparer($(this).index()));
      this.asc = !this.asc;
      if (!this.asc){
        rows = rows.reverse();
      }
      for (var i = 0; i < rows.length; ++i){
        table.append(rows[i]);
      }
    });
    // dedicates numbers and positions  of rows, compare input data, is needed for sorting and comparing
    function comparer(index) {
          return function(a, b) {
                var valA = getCellValue(a, index),
        valB = getCellValue(b, index);
                return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB);
          }
    }
    // Getting data from rows TD to sort
    function getCellValue(row, index){
     return $(row).children('td').eq(index).html() ;
   }

  // FILTER 
  $('table').each(function(){
        var table = $(this);
        var headers = table.find('th').length;
        var filterrow = $('<tr>').insertAfter($(this).find('th:last()').parent());
    // Creating inputs without value data, and with loop search inputed data if KEYUP
        for (var i = 0; i < headers; i++){
              filterrow.append($('<th>').append($('<input>').attr('type','text').keyup(function(){
              table.find('tr').show();
              filterrow.find('input[type=text]').each(function(){
              var index = $(this).parent().index() + 1;
              var filter = $(this).val() != '';
              $(this).toggleClass('filtered', filter);
              if (filter){
               var el = 'td:nth-child('+index+')';
               var criteria = ":contains('"+$(this).val()+"')";
               table.find(el+':not('+criteria+')').parent().hide();
                   }
                  });
             })));
        }



// Add clear (CLEAR) FILTER btn 
$(document).on('click', '.clear-filter', function(){
 filterrow.find('input[type=text]').val('').toggleClass('filtered', false);
 table.find('tr').show();
});

});


// Select all Checkboxes
$(".check_all").change(function () {
  $("input:checkbox").prop('checked', $(this).prop("checked"));
});

  // Button to show and Show Table filter
    $('.clear-filter').hide();
  $(document).on('click', '.show-filter', function(){
    $('table thead tr').eq(1).show();
    $('.hide-filter').show();
    $('.clear-filter').show();
    $('.show-filter').hide();
  });

   // Button to show and Hide Table filter
   $('.hide-filter').hide();
   $(document).on('click', '.hide-filter', function(){
    $('table thead tr').eq(1).hide();
    $('.show-filter').show();
    $('.hide-filter').hide();
    $('.clear-filter').hide();

  });

  // Hide input in "Action" and "Select" row
  var before_last_th_input = $('table thead tr th input').eq(-1);
  before_last_th_input.remove();
  var first_th_input = $('table thead tr th input').eq(1);
  first_th_input.remove();

  // Checkboxes check and uncheck ALL

});