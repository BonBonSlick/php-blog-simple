<?php
ob_start();
session_start();
  
ini_set('session.gc_maxlifetime', 1);
ini_set('session.cookie_lifetime', 1);
// ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] .'../sessions/');
// setcookie( 'old_session', $_COOKIE['PHPSESSID'], time()+2592000);

define('DBHOST', 'localhost');
define('DBUSER', 'admin');
define('DBPASS', 'admin');
define('DBNAME', 'blog');

try {
	$optimize = array(
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
		);
	$blog_bd = new PDO ('mysql:host=' . DBHOST . ';dbname=' . DBNAME, DBUSER, DBPASS, $optimize);
} catch (PDOException $e) {
	echo "Some ERROR with connecting to DataBase!!!" . $e->getMessage();
}
