<?php

function show_table(){
	try{

		require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');

		$total  = $blog_bd->query( "SELECT COUNT( cat_id ) as rows FROM categories")->fetch(PDO::FETCH_OBJ);
		$perpage = intval( $_SESSION['post_per_page'] );
		if ( empty ( $perpage ) ) {
			$perpage = 5;
		}
		$all_posts  = $total->rows;
		$all_pages  = ceil($all_posts / $perpage);
		$get_pages = isset($_GET['page']) ? $_GET['page'] : 1;
		$data = array(
			'options' => array(
				'default'   => 1,
				'min_range' => 1,
				'max_range' => $all_pages
				)
			);
		$number = trim($get_pages);
		$number = filter_var($number, FILTER_VALIDATE_INT, $data);
		$range  = $perpage * ($number - 1);

		$prev = $number - 1;
		$next = $number + 1;

		$db_con = $blog_bd->prepare('SELECT * FROM categories ORDER BY cat_id DESC LIMIT :limit, :perpage ');
		$db_con->bindParam(':perpage', $perpage, PDO::PARAM_INT);
		$db_con->bindParam(':limit', $range, PDO::PARAM_INT);
		$db_con->execute();
		while ( $row = $db_con->fetch()) {
			$cat_id = $row['cat_id'];
			$cat_name = $row['cat'];

			echo "
			<tr>
				<td><input type='checkbox'  name='checkme[]'   value='" .  $cat_id . "' />
					Select to Delete
				</td>
				<td> "  . $cat_id . " </td>
				<td> "  . $cat_name . " </td>

				<td>
					<a href='http://blog/view/admin/post_control/edit_cat.php?cat_id_to_edit=" .  $cat_id . "'>
						<div class='btn btn-success btn-edit'>Edit</div>
					</a>
					<button type='submit' name='btn-remove' value='" .  $cat_id . "'class='btn btn-danger btn-remove'>Remove</button>

				</td>
			</tr>
			";

		}
	}  catch (PDOException $e) {
		echo $e->getMessage();
	}
}


// Show pagination and post limit per page
function pagination(){

	require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');

	$total  = $blog_bd->query( "SELECT COUNT( cat_id ) as rows FROM categories")->fetch(PDO::FETCH_OBJ);

	if ( isset($_SESSION['post_per_page']) && empty ( $_SESSION['post_per_page'] ) ){
		$_SESSION['post_per_page'] = 1;
	}
	$perpage = intval( $_SESSION['post_per_page'] );
	$all_posts  = $total->rows;
	$all_pages  = ceil($all_posts / $perpage);
	$get_pages = isset($_GET['page']) ? $_GET['page'] : 1;
	$data = array(
		'options' => array(
			'default'   => 1,
			'min_range' => 1,
			'max_range' => $all_pages
			)
		);
	$number = trim($get_pages);
	$number = filter_var($number, FILTER_VALIDATE_INT, $data);
	$range  = $perpage * ($number - 1);

	$prev = $number - 1;
	$next = $number + 1;

	$db_con = $blog_bd->prepare('SELECT * FROM categories LIMIT :limit, :perpage');
	$db_con->bindParam(':perpage', $perpage, PDO::PARAM_INT);
	$db_con->bindParam(':limit', $range, PDO::PARAM_INT);
	$db_con->execute();
	$result = $db_con->fetchAll();

	if($result && count($result) > 0){
					# first page
		if($number <= 1){
			echo "<div class='btn'><b>&laquo; BACK </b></div><a href=\"?page=$next\">  <div class='btn btn-success'><b> NEXT &raquo;</b></div> </a>";
		}
					# last page
		elseif($number >= $all_pages){
			echo "<a href=\"?page=$prev\"><div class='btn btn-primary'><b>&laquo; BACK </b></div></a> <div class='btn'><b> NEXT &raquo;</b></div>";
		}
					# in range
		else{
			echo "<a href=\"?page=$prev\"><div class='btn btn-primary'><b>&laquo; BACK </b></div></a><a href=\"?page=$next\">  <div class='btn btn-success'><b> NEXT &raquo;</b></div>  </a>";
		}
// var_dump($_GET);
		echo " <b> Page: ( $number )  of ( $all_pages ) </b>";

	}
	else{
		echo "<p>No results found.</p>";
	}


}

//Buttons controllers 

function buttons_controls(){

	require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');

// Posts per page button
	if ( isset ( $_POST['submit_post_per_page'] ) ){
		$post_per_page = $_POST['post_per_page'];
		$_SESSION['post_per_page'] = $post_per_page;
		header('location:posts_cats.php');
	}



//Delete checkbxed
	$posts_to_delete = $_POST['checkme'];  
	if ( isset ( $_POST['btn-delete-all'] ) && $posts_to_delete != null && !empty( $posts_to_delete ) ) {
		foreach( $posts_to_delete as $delete_cat_id) {
			$db_con = $blog_bd->prepare('DELETE FROM posts WHERE post_id =' . $delete_cat_id );
			$db_con->execute();
		}
		header('location:all_posts.php');
	}
// Remove BTN
	if( isset( $_POST['btn-remove'] ) ){
		$delete_cat_id =  $_POST['btn-remove'];
		try {
			$db_con = $blog_bd->prepare('DELETE FROM categories WHERE cat_id =' . $delete_cat_id);
			$db_con->execute();
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
		header('location:posts_cats.php');
	}

// Search field button
	// if ( isset ( $_GET['submit_search_data'] ) ) {

	// 	var_dump($_POST['search_data_field']);exit;
	// 	header('location:search_results.php?search_data=');

	// }


// Edit BTN
// if( isset($_POST['btn-edit'])){
// 	$edit_post_id = $_POST['btn-edit'];
// 	$_SESSION['post_id_to_edti'] = $edit_post_id;
// 	header('location:edit_post.php');
// }

}