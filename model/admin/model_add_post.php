<?php 

function submit_post(){
	require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');

	if ( isset($_POST['submit_add_post'])){


		if(isset($_FILES['image'])){
			$errors= array();
			$file_name = $_FILES['image']['name'];
			$file_size = $_FILES['image']['size'];
			$file_tmp = $_FILES['image']['tmp_name'];
			$file_type = $_FILES['image']['type'];
			$file_ext=strtolower( end( explode('.',$_FILES['image']['name'])));

			$expensions= array("jpeg","jpg","png");

			if(in_array($file_ext,$expensions)=== false){
				$errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}

			if($file_size > 2097152) {
				$errors[]='File size must be excately 2 MB';
			}

			if( empty( $errors ) == true) {
				move_uploaded_file($file_tmp, $_SERVER['DOCUMENT_ROOT'] . "/assets/img/" . $file_name);

			}else{
				print_r($errors);
			}
		}


		try {
		// Post information
			$db_con = $blog_bd->prepare('INSERT INTO posts ( post_id, post_title, post_slug, post_cont, post_pic_url, post_date ) VALUES ( post_id, :post_title,  :post_slug,  :post_cont, :post_pic_url, :post_date ) ');
			$db_con->execute(array(
				':post_title' => $_POST['new_post_title'],
				':post_slug' => md5($_POST['new_post_slug'] . uniqid()),
				':post_cont' => $_POST['new_post_cont'],
				':post_pic_url' => "/assets/img/" . $file_name ,
				':post_date' => date('Y-m-d H:i:s')
				));

			$new_post_id =  $blog_bd->lastInsertId();

		// Post Category
			$db_con_2 = $blog_bd->prepare('INSERT INTO post_cats ( id, cat_name, post_id ) VALUES ( id, :cat_name,  :post_id ) ');
			$db_con_2->execute(array(
				':cat_name' => $_POST['new_post_cat'],
				':post_id' =>  $new_post_id 
				));
		} catch (PDOException $e) {
			$e->getMessage();
		}
		header('location:all_posts.php');
	}

}