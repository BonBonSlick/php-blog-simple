<?php 

function submit_edit(){
	require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');
	$post_id = $_GET['post_id_to_edit'];
	
	if ( isset ( $_POST['submit_edit_post'] ) ) {
		$new_post_title = strval($_POST['new_post_title']);
		$new_post_slug = strval($_POST['new_post_slug']);
		$new_post_cont = strval($_POST['new_post_cont']);
		$new_post_cat = strval($_POST['new_post_cat']);

		if(isset($_FILES['img_inp'])){
			$errors= array();
			$file_name = $_FILES['img_inp']['name'];
			$file_size = $_FILES['img_inp']['size'];
			$file_tmp = $_FILES['img_inp']['tmp_name'];
			$file_type = $_FILES['img_inp']['type'];
			$file_ext=strtolower(end(explode('.',$_FILES['img_inp']['name'])));

			$expensions= array("jpeg","jpg","png");

			if(in_array($file_ext,$expensions)=== false){
				$errors[]="extension not allowed, please choose a JPEG or PNG file.";
			}

			if($file_size > 2097152) {
				$errors[]='File size must be excately 2 MB';
			}

			if( empty( $errors ) == true) {
				move_uploaded_file($file_tmp, $_SERVER['DOCUMENT_ROOT'] . "/assets/img/" . $file_name);
				$new_post_pic_url = strval( "/assets/img/" . $file_name ); 

			}else{
				print_r($errors);
			}
		}
 		if ( $_FILES['img_inp'] ){
			$new_post_pic_url = $_POST['old_post_pic'];
		}
		try {
			$db_con_4 = $blog_bd->query('UPDATE posts SET post_title = "' . $new_post_title  . '", post_slug = "' . $new_post_slug  . '", post_cont = "' . $new_post_cont  . '", post_pic_url = "' . $new_post_pic_url  . '" WHERE post_id = ' . $post_id );

			$db_con_5 = $blog_bd->prepare('SELECT * FROM post_cats WHERE post_id = ' . $post_id);
			$db_con_5->execute();
			$row = $db_con_5->fetch();

			if ( empty( $row ) ){
				$db_con_6 = $blog_bd->prepare('INSERT INTO `post_cats`(`post_id`) VALUES (:post_id)');
				$db_con_6->execute(array(
					':post_id' => $post_id
					));
			}

			$db_con_7 = $blog_bd->query('UPDATE post_cats SET cat_name = "' . $new_post_cat . '"  WHERE post_id = ' . $post_id );
		} catch (PDOException $e) {
			$e->getMessage();
		}
		header('location:edit_post.php?post_id_to_edit=' . $post_id);
	}

}