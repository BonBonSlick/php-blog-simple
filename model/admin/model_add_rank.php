<?php 

function submit_post(){
	require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');

	if ( isset($_POST['submit_edit_post'])){
		try {
		// Post information
			$db_con = $blog_bd->prepare('INSERT INTO posts ( post_id, post_title, post_slug, post_cont, post_date ) VALUES ( post_id, :post_title,  :post_slug,  :post_cont, :post_date ) ');
			$db_con->execute(array(
				':post_title' => $_POST['new_post_title'],
				':post_slug' => $_POST['new_post_slug'],
				':post_cont' => $_POST['new_post_cont'],
				':post_date' => date('Y-m-d H:i:s')
				));

			$new_post_id =  $blog_bd->lastInsertId();

		// Post Category
			$db_con_2 = $blog_bd->prepare('INSERT INTO post_cats ( id, cat_name, post_id ) VALUES ( id, :cat_name,  :post_id ) ');
			$db_con_2->execute(array(
				':cat_name' => $_POST['new_post_cat'],
				':post_id' =>  $new_post_id 
				));
		} catch (PDOException $e) {
			$e->getMessage();
		}
		header('location:all_posts.php');
	}

}