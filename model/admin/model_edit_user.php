<?php 

function submit_edit(){
	require($_SERVER['DOCUMENT_ROOT'] . '/model/db_connect.php');
	$post_id = $_GET['post_id_to_edit'];
	
	if ( isset ( $_POST['submit_edit_post'] ) ) {
		$new_post_title = strval($_POST['new_post_title']);
		$new_post_slug = strval($_POST['new_post_slug']);
		$new_post_cont = strval($_POST['new_post_cont']);
		$new_post_cat = strval($_POST['new_post_cat']);
		try {
			$db_con_4 = $blog_bd->query('UPDATE posts SET post_title = "' . $new_post_title  . '", post_slug = "' . $new_post_slug  . '", post_cont = "' . $new_post_cont  . '" WHERE post_id = ' . $post_id );

			$db_con_5 = $blog_bd->prepare('SELECT * FROM post_cats WHERE post_id = ' . $post_id);
			$db_con_5->execute();
			$row = $db_con_5->fetch();

			if ( empty( $row ) ){
				$db_con_6 = $blog_bd->prepare('INSERT INTO `post_cats`(`post_id`) VALUES (:post_id)');
				$db_con_6->execute(array(
					':post_id' => $post_id
					));
			}

			$db_con_7 = $blog_bd->query('UPDATE post_cats SET cat_name = "' . $new_post_cat . '"  WHERE post_id = ' . $post_id );
		} catch (PDOException $e) {
			$e->getMessage();
		}
		header('location:edit_post.php?post_id_to_edit=' . $post_id);
	}

}